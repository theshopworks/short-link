<?php

namespace Shopworks\ShortLink;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $table = 'resources';

    protected $fillable = [
        'hash',
        'url',
    ];

    public function hit(): void
    {
        $this->hits++;
        $this->save();
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function scopeByUrl(Builder $query, string $url): Builder
    {
        return $query->where('url', $url);
    }
}
