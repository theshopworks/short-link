<?php

namespace Shopworks\ShortLink\Services;

use Illuminate\Contracts\Config\Repository;
use Shopworks\ShortLink\Resource;

class ResourceService
{
    private $config;

    public function __construct(Repository $config)
    {
        $this->config = $config;
    }

    public function create(string $url): Resource
    {
        $resource = Resource::byUrl($url)->first();

        if (!$resource) {
            $resource = Resource::create([
                'url' => $url,
            ]);
        }

        return $resource;
    }

    public function find(string $hash):? Resource
    {
        return Resource::query()->where('hash', $hash)->first();
    }

    public function generateLink(Resource $resource): string
    {
        return sprintf('%s/%s', $this->config->get('app.url'), $resource->getHash());
    }
}
