<?php

namespace Shopworks\ShortLink\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Contracts\Config\Repository;

class Authenticate
{
    protected $auth;
    private $config;

    public function __construct(Auth $auth, Repository $config)
    {
        $this->auth = $auth;
        $this->config = $config;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->config->get('app.authentication') === false) {
            return $next($request);
        }

        if ($this->auth->guard($guard)->guest()) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
