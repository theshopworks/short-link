<?php

namespace Shopworks\ShortLink\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Shopworks\ShortLink\Services\ResourceService;
use Symfony\Component\HttpFoundation\Response;

class CreateController extends Controller
{
    private $resourceService;

    public function __construct(ResourceService $resourceService)
    {
        $this->resourceService = $resourceService;
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validate($request, [
            'url' => 'url|required',
        ]);

        $url = trim($request->input('url'), '/');

        $resource = $this->resourceService->create($url);

        return new JsonResponse(
            [
                'hash' => $resource->getHash(),
                'url' => $this->resourceService->generateLink($resource),
            ],
            Response::HTTP_CREATED
        );
    }
}
