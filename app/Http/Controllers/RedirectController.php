<?php

namespace Shopworks\ShortLink\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Shopworks\ShortLink\Services\ResourceService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RedirectController
{
    private $resourceService;

    public function __construct(ResourceService $resourceService)
    {
        $this->resourceService = $resourceService;
    }

    public function __invoke(string $hash)
    {
        $resource = $this->resourceService->find($hash);

        if (!$resource) {
            throw new NotFoundHttpException();
        }

        $resource->hit();

        return new RedirectResponse($resource->getUrl());
    }
}
