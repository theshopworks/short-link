<?php

namespace Shopworks\ShortLink\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Routing\Router;

final class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'Shopworks\ShortLink\Http\Controllers';

    public function boot(): void
    {
        $this->app->router->group([
            'namespace' => $this->namespace,
        ], function (Router $router) {
            $router->post('/api/create', [
                'middleware' => 'auth',
                'uses' => 'CreateController',
            ]);

            $router->get('/{hash}', [
                'uses' => 'RedirectController',
            ]);
        });
    }
}
