<?php

namespace Shopworks\ShortLink\Providers;

use Hashids\Hashids;
use Illuminate\Support\ServiceProvider;
use Shopworks\ShortLink\Resource;

class AppServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Resource::created(function (Resource $resource) {
            $hashids = $this->app->make(Hashids::class);

            $resource->update(['hash' => $hashids->encode($resource->id)]);
        });
    }

    public function register(): void
    {
        $this->app->bind(Hashids::class, function () {
            $config = $this->app['config']->get('hashids');

            return new Hashids($config['salt'], $config['min_length']);
        });
    }
}
