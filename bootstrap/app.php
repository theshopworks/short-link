<?php

require_once __DIR__ . '/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {

}

$app = new Laravel\Lumen\Application(
    realpath(__DIR__ . '/../')
);

$app->withFacades();

$app->withEloquent();

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    Shopworks\ShortLink\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    Shopworks\ShortLink\Console\Kernel::class
);

$app->routeMiddleware([
    'auth' => Shopworks\ShortLink\Http\Middleware\Authenticate::class,
]);

$app->configure('hashids');
$app->configure('app');

$app->register(Shopworks\ShortLink\Providers\AppServiceProvider::class);
$app->register(Shopworks\ShortLink\Providers\RouteServiceProvider::class);
$app->register(Shopworks\ShortLink\Providers\AuthServiceProvider::class);

return $app;
