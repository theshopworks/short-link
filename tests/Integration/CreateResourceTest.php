<?php

namespace Tests\Integration;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateResourceTest extends TestCase
{
    public function test_it_creates_fresh_resource(): void
    {
        config(['app.url' => 'http://short.me']);

        $this
            ->json('POST', '/api/create', [
                'url' => 'http://dev.shopworks',
            ])
            ->assertResponseStatus(Response::HTTP_CREATED);

        $response = $this->response;

        $body = $response->getContent();

        $this->assertJson($body);
        $content = json_decode($body, true);
        $this->assertArrayHasKey('hash', $content);
        $this->assertArrayHasKey('url', $content);

        $this->assertContains('http://short.me', $content['url']);
    }
}
