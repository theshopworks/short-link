<?php

namespace Tests\Unit\Services;

use Illuminate\Contracts\Config\Repository;
use Mockery\Mock;
use Shopworks\ShortLink\Resource;
use Shopworks\ShortLink\Services\ResourceService;
use Tests\TestCase;

class ResourceServiceTest extends TestCase
{
    /** @var ResourceService */
    protected $service;
    /** @var Repository|Mock */
    protected $config;

    public function setUp(): void
    {
        parent::setUp();

        $this->config = \Mockery::mock(Repository::class);
        $this->service = new ResourceService($this->config);
    }

    public function test_it_creates_resource(): void
    {
        $url = 'http://www.google.com';
        $resource = $this->service->create($url);

        $this->assertInstanceOf(Resource::class, $resource);
        $this->assertEquals($url, $resource->getUrl());
    }

    public function test_it_does_not_create_resource_if_same_link_exists(): void
    {
        $url = 'http://www.google.com';
        $resource = $this->service->create($url);
        $resourceB = $this->service->create($url);

        $this->assertInstanceOf(Resource::class, $resource);
        $this->assertInstanceOf(Resource::class, $resourceB);

        $this->assertEquals($resource->id, $resourceB->id);
    }

    public function test_it_finds_resource_by_hash(): void
    {
        $resource = $this->service->create('http://www.test.com');

        $result = $this->service->find($resource->getHash());

        $this->assertEquals($result->id, $resource->id);
    }

    public function test_it_generates_link_with_domain(): void
    {
        $resource = $this->service->create('http://www.url.com');

        $this->config->shouldReceive('get')->with('app.url')->andReturn('http://short.me');
        $link = $this->service->generateLink($resource);

        $this->assertEquals('http://short.me/' . $resource->getHash(), $link);
    }
}
