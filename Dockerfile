FROM php:7.1-apache

RUN apt-get update && \
    apt-get install -y git libpng-dev zlib1g-dev libssl-dev libffi-dev openssh-client python-dev python-setuptools zip unzip gnupg2 && \
    a2enmod rewrite

RUN docker-php-ext-install zip pdo_mysql bcmath

COPY composer.lock composer.json /var/www/html/

WORKDIR /var/www/html

RUN php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php && \
    php composer-setup.php && php -r "unlink('composer-setup.php');" && \
    php composer.phar install --no-dev --no-scripts && \
    rm composer.phar && \
    sed -i "/DocumentRoot \/var\/www\/html/c\DocumentRoot \/var\/www\/html\/public/" /etc/apache2/sites-available/000-default.conf && \
    mkdir -p /var/www/html/storage

COPY . /var/www/html/

RUN echo "APP_ENV=production" > /var/www/html/.env && \
    echo "APP_TIMEZONE=UTC" >> /var/www/html/.env && \
    echo "APP_URL=" >> /var/www/html/.env && \

    echo "DB_CONNECTION=mysql" >> /var/www/html/.env && \
    echo "DB_HOST=mysql" >> /var/www/html/.env && \
    echo "DB_PORT=3306" >> /var/www/html/.env && \
    echo "DB_DATABASE=short_links" >> /var/www/html/.env && \
    echo "DB_USERNAME=" >> /var/www/html/.env && \
    echo "DB_PASSWORD=" >> /var/www/html/.env && \

    echo "CACHE_DRIVER=file" >> /var/www/html/.env && \

    echo "HASH_IDS_SALT=" >> /var/www/html/.env && \
    echo "HASH_IDS_MIN_LENGTH=" >> /var/www/html/.env && \

    echo "#!/bin/bash" > /usr/local/bin/entrypoint.d && \
    echo "/usr/local/bin/php /var/www/html/artisan --force migrate" >> /usr/local/bin/entrypoint.d && \
    echo "apache2-foreground" >> /usr/local/bin/entrypoint.d && \

    chmod +x /usr/local/bin/entrypoint.d


RUN chown -R www-data:www-data /var/www/html/storage

ENTRYPOINT ["entrypoint.d"]