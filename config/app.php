<?php

return [
    'url' => trim(env('APP_URL'), '/'),
    'authentication' => env('AUTHENTICATION', false),
];
