<?php

return [
    'salt' => env('HASH_IDS_SALT'),
    'min_length' => env('HASH_IDS_MIN_LENGTH', 4),
];

